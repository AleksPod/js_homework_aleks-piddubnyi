// Теоретичні питання
//
//1) Описати своїми словами навіщо потрібні функції у програмуванні.
// Функція у програмуванні допомогає реалізувати певний алгоритм дій ( який у подальшому можна використати безліч разів).
//2) Описати своїми словами, навіщо у функцію передавати аргумент.
// Завдяки аргументам ми можимо виконати нашу функцію у будь якому місця області видимості, також завдялки аргумаентам
// наша функція стає більш гибкою і ми можемо передавати будь які значення(змінні) які у прямій послідовності передадуть
// нашу функцію.
// 3) Що таке оператор return та як він працює всередині функції?
//Оператор return зпаершає виконання нашої поточної функції, та вертає її значення.

const firstNumb = prompt("Enter your First Number");
const secondNumb = prompt("Enter your Second Number");
const calculateOperation = prompt("Enter type of your math operation");

function calculate(a, b, c){
    if (c === '+'){
        return parseInt(a) + parseInt(b);
    }
    else if (c === '-'){
        return parseInt(a) - parseInt(b);
    }
    else if (c === '*'){
        return parseInt(a) * parseInt(b);
    }
    else if (c === '/'){
        return parseInt(a) / parseInt(b);
    }
    else{
        return false;
    }
}
console.log(calculate(firstNumb,secondNumb,calculateOperation));